package com.yzg.mediator;

/**
 * @author misterWei
 * @create 2020年04月05号:02点34分
 * @mailbox forcontinue@163.com
 *
 * 房子中介 核心
 */
public class HouseMediator extends AbsMediator {
      private AbsHave absHave;

    public HouseMediator() {
        absHave = new HouseMaster();
        absHaveMap.put("北京房东",absHave);
    }

    @Override
    protected String acceptMes(String area,int ...types) {

        return absHaveMap.get(area).sendMessage(types[0]);
    }
}
