package com.yzg.mediator;

/**
 * @author misterWei
 * @create 2020年04月05号:02点17分
 * @mailbox forcontinue@163.com
 */
public class TestMain {

    public static void main(String[] args) {

        AbsMediator mediator = new HouseMediator();
        //使用房子中介找到小房子
        System.out.println(mediator.acceptMes("北京房东", 2));
        //使用房子中介找到大房子

        System.out.println(mediator.acceptMes("北京房东", 1));
    }

}
