package com.yzg.mediator;

import java.util.HashMap;
import java.util.Map;

/**
 * @author misterWei
 * @create 2020年04月05号:02点24分
 * @mailbox forcontinue@163.com
 * 中介者
 */
public abstract class AbsMediator {
    protected Map<String,AbsHave> absHaveMap;

    public AbsMediator() {
        absHaveMap = new HashMap<>();
    }

    protected abstract String acceptMes(String area,int ...types);
}
