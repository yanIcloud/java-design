package com.yzg.mediator;

import java.util.HashMap;
import java.util.Map;

/**
 * @author misterWei
 * @create 2020年04月05号:02点25分
 * @mailbox forcontinue@163.com
 */
public  class HouseMaster extends AbsHave{
    BigHouse bigHouse;
    SmallHouse smallHouse;
    Map resultHouse ;


    public HouseMaster() {
        bigHouse = new BigHouse();
        smallHouse = new SmallHouse();
        resultHouse = new HashMap();
        resultHouse.put(1,bigHouse);
        resultHouse.put(2,smallHouse);
    }

    @Override
    protected String sendMessage(Integer status) {
        return resultHouse.containsKey(status) ? "存在" : "不存在";
    }

   class BigHouse{
        public String name;
        public BigHouse(){
            name = "大房子";
        }
   }
    class SmallHouse{
        public String name;
        public SmallHouse(){
            name = "小房子";
        }
    }
}
