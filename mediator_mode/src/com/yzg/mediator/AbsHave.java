package com.yzg.mediator;

/**
 * @author misterWei
 * @create 2020年04月05号:02点25分
 * @mailbox forcontinue@163.com
 *
 * 此类是对中介者类开放的
 */
public abstract class AbsHave {


    protected abstract String sendMessage(Integer status);
}
