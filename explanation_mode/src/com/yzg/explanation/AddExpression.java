package com.yzg.explanation;

import java.util.HashMap;

/**
 * �ӷ�������
 * @author Administrator
 *
 */
public class AddExpression extends SymbolExpression  {

	public AddExpression(Expression left, Expression right) {
		super(left, right);
	}

	//var HashMap {a=10,b=20}..
	@Override
	public int interpreter(HashMap<String, Integer> var) {
		//super.left.interpreter(var)
		//super.right.interpreter(var):
		return super.left.interpreter(var) + super.right.interpreter(var);
	}
}
