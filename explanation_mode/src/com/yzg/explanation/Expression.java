package com.yzg.explanation;

import java.util.HashMap;

/**
 * 抽象解释器范本
 * 
 * @author Administrator
 *
 */
public abstract class Expression {
	// a + b - c
	// HashMap {a=10, b=20}
	public abstract int interpreter(HashMap<String, Integer> var);
}
