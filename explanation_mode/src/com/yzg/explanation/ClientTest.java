package com.yzg.explanation;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;

public class ClientTest {

	/**
	 * 步骤说明
	 * @param args
	 * @throws IOException
	 * getExpStr() 方法获取 a+b  等这些运算符 返回就是 a+b
	 *  getValue() 获取 a的值和b的值 返回Map {a=10,b=20}
	 *   Calculator 实例,用来获取使用那个解释器
	 *    run 使用解释器去执行该数据 其参数就是 getValue() 方法返回的Map
	 *
	 */
	public static void main(String[] args) throws IOException {
		String expStr = getExpStr(); // a+b
		HashMap<String, Integer> var = getValue(expStr);// var {a=10, b=20}
		Calculator calculator = new Calculator(expStr);
		calculator.run(var);
	}

	public static String getExpStr() throws IOException {
		return (new BufferedReader(new InputStreamReader(System.in))).readLine();
	}

	public static HashMap<String, Integer> getValue(String expStr) throws IOException {
		HashMap<String, Integer> map = new HashMap<>();

		for (char ch : expStr.toCharArray()) {
			if (ch != '+' && ch != '-') {
				if (!map.containsKey(String.valueOf(ch))) {
					String in = (new BufferedReader(new InputStreamReader(System.in))).readLine();
					map.put(String.valueOf(ch), Integer.valueOf(in));
				}
			}
		}

		return map;
	}
}
