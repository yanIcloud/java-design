package com.yzg.explanation;

import java.util.HashMap;

/**
 *
 * @author Administrator
 *
 * a + c
 * left: a
 * right: c
 *
 */
public class SymbolExpression extends Expression {

	protected Expression left;
	protected Expression right;

	public SymbolExpression(Expression left, Expression right) {
		this.left = left;
		this.right = right;
	}

	//该方法在非终结者符号解释器中细节是让其子类实现的
	@Override
	public int interpreter(HashMap<String, Integer> var) {
		// TODO Auto-generated method stub
		return 0;
	}
}
