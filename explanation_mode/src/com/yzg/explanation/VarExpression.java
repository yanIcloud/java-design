package com.yzg.explanation;

import java.util.HashMap;


/**
 * �����Ľ�����
 * @author Administrator
 *
 */
public class VarExpression extends Expression {

	private String key; // key=a,key=b,key=c

	public VarExpression(String key) {
		this.key = key;
	}

	// var HashMap{a=10, b=20}
	// interpreter
	@Override
	public int interpreter(HashMap<String, Integer> var) {
		return var.get(this.key);
	}
}
