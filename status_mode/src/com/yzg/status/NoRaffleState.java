package com.yzg.status;

/**
 * 没有奖品状态
 * @author Administrator
 *
 */
public class NoRaffleState extends State {

    RaffleActivity activity;

    public NoRaffleState(RaffleActivity activity) {
        this.activity = activity;
    }

    @Override
    public void deductMoney() {
        activity.setState(activity.getCanRaffleState());
    }

    @Override
    public boolean raffle() {
        return false;
    }

    @Override
    public void dispensePrize() {
        System.out.println("没有抽奖资格");
    }
}
