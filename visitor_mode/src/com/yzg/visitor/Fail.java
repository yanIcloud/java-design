package com.yzg.visitor;

/**
 * @author misterWei
 * @create 2020年04月04号:02点02分
 * @mailbox forcontinue@163.com
 *
 * 访问者实现
 */
public class Fail extends Action {
    @Override
    protected void getManResult(Man man) {
        System.out.println("男人 失败!");

    }

    @Override
    protected void getWomanResult(Woman woman) {
        System.out.println("女人 失败!");

    }
}
