package com.yzg.visitor;

/**
 * @author misterWei
 * @create 2020年04月04号:01点47分
 * @mailbox forcontinue@163.com
 *
 * 打分系统评测
 */
public class TestMain {

    public static void main(String[] args) {

        ObjectElements objectElements = new ObjectElements();

        objectElements.addPerson(new Woman());
        objectElements.addPerson(new Man());
        objectElements.attchem(new Success());
        objectElements.attchem(new Fail());
    }
}
