package com.yzg.visitor;

import java.util.LinkedList;
import java.util.List;

/**
 * @author misterWei
 * @create 2020年04月04号:02点03分
 * @mailbox forcontinue@163.com
 * 充当门面调用
 */
public class ObjectElements {
    final List<Person>  persons = new LinkedList<>();

    public void addPerson(Person person){
        persons.add(person);

    }

    public void delPerson(Person person){
        persons.remove(person);

    }

    /**
     * 得到结果
     */
    public void attchem(Action action){
        for (Person person : persons) {
           person.accept(action);
        }

    }
}
