package com.yzg.visitor;

/**
 * @author misterWei
 * @create 2020年04月04号:02点01分
 * @mailbox forcontinue@163.com
 *
 * 访问者实现
 */
public class Success extends Action {
    @Override
    protected void getManResult(Man man) {
        System.out.println("男人 成功!");
    }

    @Override
    protected void getWomanResult(Woman woman) {
        System.out.println("女人 成功!");

    }
}
