package com.yzg.visitor;

/**
 * @author misterWei
 * @create 2020年04月04号:01点54分
 * @mailbox forcontinue@163.com
 * 访问者抽象
 * 这就是抽象的一个打分评测
 */
public abstract class Action {
    protected abstract void getManResult(Man man);
    protected abstract void getWomanResult(Woman woman);

}
