package com.yzg.visitor;

/**
 * @author misterWei
 * @create 2020年04月04号:01点59分
 * @mailbox forcontinue@163.com
 *
 * element 抽象
 */
public abstract class Person {
   protected   abstract void accept(Action action);
}
