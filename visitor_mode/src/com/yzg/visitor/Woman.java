package com.yzg.visitor;

/**
 * @author misterWei
 * @create 2020年04月04号:01点59分
 * @mailbox forcontinue@163.com
 *  * element 具体实现
 */
public class Woman extends Person {

    /**
     * 获取访问者的信息
     * @param action
     */
    @Override
    protected void accept(Action action) {
     action.getWomanResult(this);
    }
}
