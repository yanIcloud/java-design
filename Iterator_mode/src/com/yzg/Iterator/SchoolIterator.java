package com.yzg.Iterator;

import java.util.Iterator;

/**
 * @author misterWei
 * @create 2020年04月04号:20点07分
 * @mailbox forcontinue@163.com
 */
public class SchoolIterator implements Iterator {
    private SchoolCollege[] schools;
    private int index;

    public SchoolIterator(SchoolCollege[] schools) {
        this.schools = schools;
    }

    /**
     * 判断是否有下一个元素
     * @return
     */
    @Override
    public boolean hasNext() {
        if (index >= schools.length){
            return false;
        }
        return true;
    }

    @Override
    public Object next() {
        SchoolCollege school = schools[index];
        index ++;
        return school;
    }
}
