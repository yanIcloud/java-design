package com.yzg.Iterator;

import java.util.Iterator;

/**
 * @author misterWei
 * @create 2020年04月04号:20点08分
 * @mailbox forcontinue@163.com
 */
public class SchoolCollege implements College {

    private String name;

    public SchoolCollege(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public Iterator createIterator() {
        SchoolCollege[] schools = new SchoolCollege[1];
            schools[0] = this;
        return new SchoolIterator(schools);
    }
}
