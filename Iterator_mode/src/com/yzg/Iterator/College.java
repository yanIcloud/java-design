package com.yzg.Iterator;

import java.util.Iterator;

/**
 * @author misterWei
 * @create 2020年04月04号:20点12分
 * @mailbox forcontinue@163.com
 */
public interface College {
    Iterator createIterator();
}
