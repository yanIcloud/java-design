package com.yzg.Iterator;

import java.util.Iterator;

/**
 * @author misterWei
 * @create 2020年04月04号:20点06分
 * @mailbox forcontinue@163.com
 *
 * 迭代器模式 示例  --> 学校  --> 专业
 */
public class TestMain {

    public static void main(String[] args) {
         SchoolCollege schoolCollege = new SchoolCollege("清华大学");
        Iterator iterator = schoolCollege.createIterator();
        while (iterator.hasNext()) {
            SchoolCollege next = (SchoolCollege) iterator.next();
            System.out.println(next.getName());
        }
        ProfessionCollege professionCollege = new ProfessionCollege();
        Iterator iterator1 = professionCollege.createIterator();
        while (iterator1.hasNext()) {
            ProfessionCollege next = (ProfessionCollege) iterator1.next();
            System.out.println(next.getName());
        }
    }
}
