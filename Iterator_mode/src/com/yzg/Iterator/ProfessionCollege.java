package com.yzg.Iterator;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * @author misterWei
 * @create 2020年04月04号:20点14分
 * @mailbox forcontinue@163.com
 */
public class ProfessionCollege implements College {
    private String name;

    public ProfessionCollege(String name) {
        this.name = name;
    }

    public ProfessionCollege() {
    }

    public String getName() {
        return name;
    }

    @Override
    public Iterator createIterator() {
        List<ProfessionCollege> professionColleges = new LinkedList<>();
        ProfessionCollege professionCollege1 = new ProfessionCollege("计算机专业");
        ProfessionCollege professionCollege2 = new ProfessionCollege("美工专业");
        professionColleges.add(professionCollege1);
        professionColleges.add(professionCollege2);
        return new ProfessionIterator(professionColleges);
    }
}
