package com.yzg.Iterator;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * @author misterWei
 * @create 2020年04月04号:20点08分
 * @mailbox forcontinue@163.com
 */
public class ProfessionIterator implements Iterator {
    private List<ProfessionCollege> professionColleges = new LinkedList<>();
     private int index;
    public ProfessionIterator(List<ProfessionCollege> professionColleges) {
        this.professionColleges = professionColleges;
    }

    @Override
    public boolean hasNext() {
        if (index >= professionColleges.size()){
            return false;
        }
        return true;
    }

    @Override
    public Object next() {
        ProfessionCollege professionCollege = professionColleges.get(index);
        index += 1;
        return professionCollege;
    }
}
