package com.yzg.memo;

import java.io.*;

/**
 * @author misterWei
 * @create 2020年04月05号:16点16分
 * @mailbox forcontinue@163.com
 *
 * 游戏角色状态
 */
public class GameRolePro implements Cloneable,Serializable {
     private GameMemo gameMemo;

     //攻击力
     private String vit;
     //血量
     private String blood;


     public GameRolePro(){
         gameMemo = new GameMemo();
     }

     //备份当前
     public int  memoThis() throws CloneNotSupportedException {
         int add = 0;
             add = gameMemo.add((GameRolePro) this.clone());
         return add;
     }
     //恢复历史
    public void restore(int index){
        GameRolePro gameRolePro = gameMemo.get(index);
        this.vit  = gameRolePro.getVit();
        this.blood  = gameRolePro.getBlood();

    }

    public void accpt(){
        System.out.println("人物攻击力为: "+this.vit+" 人物血量为+ "+this.blood);
    }

    public String getVit() {
        return vit;
    }

    public void setVit(String vit) {
        this.vit = vit;
    }

    public String getBlood() {
        return blood;
    }

    public void setBlood(String blood) {
        this.blood = blood;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        ByteArrayOutputStream bos = null;
        ObjectOutputStream oos = null;
        ByteArrayInputStream bis = null;
        ObjectInputStream ois = null;
        try {

            //序列化
            bos = new ByteArrayOutputStream();
            oos = new ObjectOutputStream(bos);
            //写出当前实例
            oos.writeObject(this);

            //反序列化
            bis = new ByteArrayInputStream(bos.toByteArray());
            ois = new ObjectInputStream(bis);
            Object cloneObj = ois.readObject();
            return (GameRolePro) cloneObj;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }finally {
            try {
                bos.close();
                oos.close();
                bis.close();
                ois.close();
            }catch (Exception ex){
                ex.printStackTrace();
            }
        }
    }
}
