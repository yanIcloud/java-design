package com.yzg.memo;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

/**
 * @author misterWei
 * @create 2020年04月05号:16点17分
 * @mailbox forcontinue@163.com
 * 游戏角色状态备忘录
 */
public class GameMemo implements Serializable{
    private List<GameRolePro> gameRolePros;
    public GameMemo(){
        gameRolePros = new LinkedList<>();
    }
    public int add(GameRolePro gameRolePro){
        gameRolePros.add(gameRolePro);
        return gameRolePros.size() - 1;
    }
    public GameRolePro get(int index){
        return gameRolePros.get(index);
    }



}
