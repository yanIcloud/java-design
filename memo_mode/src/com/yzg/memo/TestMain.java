package com.yzg.memo;

/**
 * @author misterWei
 * @create 2020年04月05号:16点15分
 * @mailbox forcontinue@163.com
 */
public class TestMain {

    public static void main(String[] args) throws CloneNotSupportedException {

        GameRolePro gameRolePro = new GameRolePro();
        gameRolePro.setVit("100");
        gameRolePro.setBlood("100");
        //第一次备份
        int i = gameRolePro.memoThis();
        gameRolePro.accpt();

        //第二次备份
        gameRolePro.setVit("50");
        gameRolePro.setBlood("50");
        int i1 = gameRolePro.memoThis();
        gameRolePro.accpt();

        //第三次备份
        gameRolePro.setVit("30");
        gameRolePro.setBlood("30");
        int i2 = gameRolePro.memoThis();
        gameRolePro.accpt();
        //恢复到第二次
        gameRolePro.restore(i1);
        gameRolePro.accpt();
    }
}
