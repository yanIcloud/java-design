package com.yzg.combination;

/**
 * @author misterWei
 * @create 2020年03月29号:02点02分
 * @mailbox forcontinue@163.com
 *
 * 组合模式以 学校 - 专业 - 系 以此说明
 * 以上三个可以理解为组合关系, 抽象出一个父类,学校组合了 专业,专业组合了系,但是这个系是个非叶子节点
 *
 */
public class TestMain {

    public static void main(String[] args) {

        AbsUniversity absUniversity = new School("清华大学","最顶尖大学");
        AbsUniversity absUniversity1 = new Profession("计算机专业","敲代码");
        absUniversity1.add(new Department("计算机科学","计算机科学"));
        absUniversity1.add(new Department("计算机应用","计算机应用"));

        absUniversity.add(absUniversity1);
        absUniversity.print();

    }
}
