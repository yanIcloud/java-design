package com.yzg.combination;

import java.util.ArrayList;
import java.util.List;

/**
 * @author misterWei
 * @create 2020年03月29号:02点07分
 * @mailbox forcontinue@163.com
 */
public class School extends AbsUniversity {

    private List<AbsUniversity> absUniversities = new ArrayList<>();

    public School(String name, String des) {
        super(name, des);
    }

    @Override
    protected void add(AbsUniversity absUniversity) {
        absUniversities.add(absUniversity);
    }

    @Override
    protected void remove(AbsUniversity absUniversity) {
        absUniversities.remove(absUniversity);
    }

    @Override
    protected void print() {
        System.out.println("---------"+super.name+"--------");
        absUniversities.forEach(absUniversity -> {
           absUniversity.print();
        });

    }
}
