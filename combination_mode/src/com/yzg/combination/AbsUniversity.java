package com.yzg.combination;

/**
 * @author misterWei
 * @create 2020年03月29号:02点05分
 * @mailbox forcontinue@163.com
 *
 * 此类就是一个描述大学的抽象类
 */
public abstract class AbsUniversity {

    protected String name;
    protected String des;

    public AbsUniversity(String name, String des) {
        this.name = name;
        this.des = des;
    }

    protected void add(AbsUniversity absUniversity){
        throw new UnsupportedOperationException();
    }

    protected void remove(AbsUniversity absUniversity){
        throw new UnsupportedOperationException();
    }

    protected abstract void print();
}
