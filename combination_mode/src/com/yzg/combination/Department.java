package com.yzg.combination;

/**
 * @author misterWei
 * @create 2020年03月29号:02点11分
 * @mailbox forcontinue@163.com
 *
 * 非叶子节点
 */
public class Department extends AbsUniversity {
    public Department(String name, String des) {
        super(name, des);
    }

    @Override
    protected void print() {
        System.out.println("---------"+super.name+"--------"+super.des);
    }
}
