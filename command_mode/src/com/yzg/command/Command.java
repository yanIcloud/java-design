package com.yzg.command;

/**
 * @author misterWei
 * @create 2020年04月04号:00点49分
 * @mailbox forcontinue@163.com
 */
public interface Command {

    void on();
    void off();
}
