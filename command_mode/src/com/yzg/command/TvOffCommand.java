package com.yzg.command;

/**
 * @author misterWei
 * @create 2020年04月04号:00点51分
 * @mailbox forcontinue@163.com
 * <p>
 * 这是一个TV 的撤销命令
 */
public class TvOffCommand implements Command {
    private TvReceiver tvReceiver;

    public TvOffCommand(TvReceiver tvReceiver) {
        this.tvReceiver = tvReceiver;
    }

    @Override
    public void on() {
        tvReceiver.stop();
    }

    @Override
    public void off() {
        tvReceiver.start();
    }
}
