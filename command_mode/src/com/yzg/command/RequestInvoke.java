package com.yzg.command;

/**
 * @author misterWei
 * @create 2020年04月04号:00点53分
 * @mailbox forcontinue@163.com
 * 实际调用者
 */
public class RequestInvoke {
    private final int offset = 5;


     Command[] tvOffCommands = new Command[offset];
    public RequestInvoke() {
        //初始化该内存
        for (int i = 0; i < offset; i++) {
            tvOffCommands[i] = new NullCommand();
        }
    }

    public void buttonConfirmOn(int index,Command command){
        tvOffCommands[index] = command;
        tvOffCommands[index].on();
    }

    public void buttonConfirmOff(int index,Command command){
        tvOffCommands[index] = command;
        tvOffCommands[index].off();
    }

}
