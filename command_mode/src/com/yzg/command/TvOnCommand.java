package com.yzg.command;

/**
 * @author misterWei
 * @create 2020年04月04号:00点51分
 * @mailbox forcontinue@163.com
 * <p>
 * 这是一个TV 的执行命令
 */
public class TvOnCommand implements Command {
    private TvReceiver tvReceiver;

    public TvOnCommand(TvReceiver tvReceiver) {
        this.tvReceiver = tvReceiver;
    }

    @Override
    public void on() {
        tvReceiver.start();
    }

    @Override
    public void off() {
        tvReceiver.stop();
    }
}
