package com.yzg.command;

/**
 * @author misterWei
 * @create 2020年04月04号:00点53分
 * @mailbox forcontinue@163.com
 * 空命令
 */
public class NullCommand implements Command{
    @Override
    public void on() {

    }

    @Override
    public void off() {

    }
}
