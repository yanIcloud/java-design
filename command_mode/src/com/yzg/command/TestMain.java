package com.yzg.command;

/**
 * @author misterWei
 * @create 2020年04月04号:00点38分
 * @mailbox forcontinue@163.com
 */
public class TestMain {

    public static void main(String[] args) {

        RequestInvoke requestInvoke = new RequestInvoke();
        TvOnCommand tvOnCommand = new TvOnCommand(new TvReceiver());
        TvOffCommand tvOffCommand = new TvOffCommand(new TvReceiver());
        //执行命令
        requestInvoke.buttonConfirmOn(0,tvOnCommand);
        requestInvoke.buttonConfirmOff(0,tvOnCommand);
        //撤销命令
        requestInvoke.buttonConfirmOn(1,tvOffCommand);

    }
}
