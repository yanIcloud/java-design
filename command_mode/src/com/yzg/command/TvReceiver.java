package com.yzg.command;

/**
 * @author misterWei
 * @create 2020年04月04号:00点50分
 * @mailbox forcontinue@163.com
 *
 * 这是一个被命令的被接受者对象
 */
public class TvReceiver {

    public void start(){
        System.out.println("Tv 打开了");
    }

    public void stop(){
        System.out.println("tv 关闭了");
    }
}
