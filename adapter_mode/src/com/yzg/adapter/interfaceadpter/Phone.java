package com.yzg.adapter.interfaceadpter;

/**
 * @author misterWei
 * @create 2020年03月22号:18点49分
 * @mailbox forcontinue@163.com
 */
public class Phone {
    private VoltageAbsAdpter voltageAbsAdpter;

    public Phone(VoltageAbsAdpter voltageAbsAdpter){
        this.voltageAbsAdpter = voltageAbsAdpter;
    }

    //充电过程
    public void process(){
        voltageAbsAdpter.voltageTar("手机");
        voltageAbsAdpter.voltage(5);
    }
}
