package com.yzg.adapter.interfaceadpter;

/**
 * @author misterWei
 * @create 2020年03月22号:18点50分
 * @mailbox forcontinue@163.com
 * 需充电
 */
public class Computer {
 private VoltageAbsAdpter voltageAbsAdpter;

    public Computer(VoltageAbsAdpter voltageAbsAdpter){
      this.voltageAbsAdpter = voltageAbsAdpter;
    }

    //充电过程
    public void process(){
        voltageAbsAdpter.voltageTar("电脑");
        voltageAbsAdpter.voltage(10);
    }
}
