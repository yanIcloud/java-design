package com.yzg.adapter.interfaceadpter;

/**
 * @author misterWei
 * @create 2020年03月22号:18点54分
 * @mailbox forcontinue@163.com
 */
public class TestMain {

    public static void main(String[] args) {
        Phone phone = new Phone(new VoltageAbsAdpter() {
            @Override
            public void voltage(int i) {
                System.out.println("充了 "+i+" V 的电");
            }

            @Override
            public void voltageTar(String tar) {
                System.out.println("目标:"+tar);
            }
        });
        phone.process();

        Computer computer = new Computer(new VoltageAbsAdpter() {
            @Override
            public void voltage(int i) {
                System.out.println("充了 "+i+" V 的电");
            }

            @Override
            public void voltageTar(String tar) {
                System.out.println("目标:"+tar);
            }
        });
        computer.process();
    }
}
