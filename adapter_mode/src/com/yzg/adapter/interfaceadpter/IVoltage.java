package com.yzg.adapter.interfaceadpter;

/**
 * @author misterWei
 * @create 2020年03月22号:18点47分
 * @mailbox forcontinue@163.com
 *
 * 关于电压的接口
 */
public interface IVoltage {

     void voltage(int i);
     void voltageTar(String tar);
}
