package com.yzg.adapter.interfaceadpter;

/**
 * @author misterWei
 * @create 2020年03月22号:18点49分
 * @mailbox forcontinue@163.com
 */
public abstract class VoltageAbsAdpter implements IVoltage {
    @Override
    public void voltage(int i) {
    }

    @Override
    public void voltageTar(String tar) {

    }
}
