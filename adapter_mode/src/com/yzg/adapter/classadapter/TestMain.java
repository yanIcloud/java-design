package com.yzg.adapter.classadapter;

/**
 * @author misterWei
 * @create 2020年03月22号:18点42分
 * @mailbox forcontinue@163.com
 */
public class TestMain {

    public static void main(String[] args) {
       Phone phone = new Phone(new VoltageAdpter());
       phone.charge();
    }
}
