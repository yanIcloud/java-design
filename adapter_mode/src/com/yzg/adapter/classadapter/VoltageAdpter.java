package com.yzg.adapter.classadapter;

/**
 * @author misterWei
 * @create 2020年03月22号:18点41分
 * @mailbox forcontinue@163.com
 * 具体适配器
 */
public class VoltageAdpter extends Voltage220V implements IVoltage5V {
    @Override
    public int output5V() {
        return output() / 44;
    }
}
