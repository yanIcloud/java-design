package com.yzg.adapter.objectadpter;

/**
 * @author misterWei
 * @create 2020年03月22号:18点41分
 * @mailbox forcontinue@163.com
 * 具体适配器
 */
public class VoltageAdpter  implements IVoltage5V {

    //继承变成了合成,关系发生改变
    private Voltage220V voltage220V;

    public VoltageAdpter(Voltage220V voltage220V){
        this.voltage220V = voltage220V;
    }
    @Override
    public int output5V() {
        return voltage220V.output() / 44;
    }
}
