package com.yzg.adapter.objectadpter;

/**
 * @author misterWei
 * @create 2020年03月22号:18点37分
 * @mailbox forcontinue@163.com
 * 使用者
 */
public class Phone {
    private IVoltage5V iVoltage5V;

    public Phone(IVoltage5V iVoltage5V){
        this.iVoltage5V = iVoltage5V;
    }

    public void charge(){
        int i = iVoltage5V.output5V();
        System.out.println("充电中:"+ i);
    }

}
