package com.yzg.adapter.objectadpter;

/**
 * @author misterWei
 * @create 2020年03月22号:18点40分
 * @mailbox forcontinue@163.com
 *
 * 适配接口
 */
public interface IVoltage5V {
    public int output5V();
}
