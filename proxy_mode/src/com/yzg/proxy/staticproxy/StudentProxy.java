package com.yzg.proxy.staticproxy;

/**
 * @author misterWei
 * @create 2020年03月31号:22点12分
 * @mailbox forcontinue@163.com
 * 静态代理
 */
public class StudentProxy implements IStudentServer {
    private IStudentServerImpl iStudentServer;

    public StudentProxy(IStudentServerImpl iStudentServer) {
        this.iStudentServer = iStudentServer;
    }

    @Override
    public void action() {
        System.out.println("代理之前");
        iStudentServer.action();
        System.out.println("代理之后");

    }
}
