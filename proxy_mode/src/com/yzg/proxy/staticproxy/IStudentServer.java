package com.yzg.proxy.staticproxy;

/**
 * @author misterWei
 * @create 2020年03月31号:22点11分
 * @mailbox forcontinue@163.com
 */
public interface IStudentServer {

    void action();
}
