package com.yzg.proxy;

import com.yzg.proxy.cglibproxy.IStudentServerImpl;
import com.yzg.proxy.cglibproxy.StudentProxy;

/**
 * @author misterWei
 * @create 2020年03月31号:21点31分
 * @mailbox forcontinue@163.com
 */
public class TestMain {
/*
    // 静态代理测试
    public static void main(String[] args) {
        StudentProxy studentProxy = new StudentProxy(new IStudentServerImpl());
        studentProxy.action();
    }*/

  /*  // 动态代理测试
    public static void main(String[] args) {
        StudentProxy studentProxy = new StudentProxy(new IStudentServerImpl());
        IStudentServer proxy = (IStudentServer) studentProxy.getProxy();
        proxy.action();
    }*/

    // cg代理测试
    public static void main(String[] args) {

        StudentProxy studentProxy = new StudentProxy(new IStudentServerImpl());
        IStudentServerImpl proxy = (IStudentServerImpl) studentProxy.getProxy();
        proxy.action();
    }
}
