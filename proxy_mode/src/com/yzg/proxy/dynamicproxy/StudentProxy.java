package com.yzg.proxy.dynamicproxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * @author misterWei
 * @create 2020年03月31号:22点12分
 * @mailbox forcontinue@163.com
 * 静态代理
 */
public class StudentProxy  {
    private Object target;

    public StudentProxy(Object target) {
        this.target = target;
    }

    public Object getProxy(){
        return Proxy.newProxyInstance(target.getClass().getClassLoader(), target.getClass().getInterfaces(), new InvocationHandler() {
            @Override
            public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {

                System.out.println("代理前");
                Object invoke = method.invoke(target, args);
                System.out.println("代理后");

                return invoke;
            }
        });
    }
}
