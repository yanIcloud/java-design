package com.yzg.proxy.cglibproxy;

import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

/**
 * @author misterWei
 * @create 2020年03月31号:22点12分
 * @mailbox forcontinue@163.com
 * 静态代理
 */
public class StudentProxy implements MethodInterceptor {
    private Object target;

    public StudentProxy(Object target) {
        this.target = target;
    }

    public Object getProxy(){
        //1 创建工具类
        Enhancer enhancer = new Enhancer();
        //2 设置父类
        enhancer.setSuperclass(target.getClass());
        //3 设置回调函数
        enhancer.setCallback(this);
        //4 创建子类对象 也就是代理对象
        return enhancer.create();

    }

    @Override
    public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {

        System.out.println("cglib 代理前");

        Object invoke = method.invoke(target, objects);

        System.out.println("cglib 代理后");


        return invoke;
    }
}
