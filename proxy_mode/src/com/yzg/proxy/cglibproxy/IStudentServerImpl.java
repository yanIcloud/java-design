package com.yzg.proxy.cglibproxy;

import com.yzg.proxy.dynamicproxy.IStudentServer;

/**
 * @author misterWei
 * @create 2020年03月31号:22点12分
 * @mailbox forcontinue@163.com
 */
public class IStudentServerImpl implements IStudentServer {
    @Override
    public void action() {
        System.out.println("学生在学习...");
    }
}
