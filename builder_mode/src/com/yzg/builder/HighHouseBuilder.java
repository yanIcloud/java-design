package com.yzg.builder;

/**
 * @author misterWei
 * @create 2020年03月22号:00点50分
 * @mailbox forcontinue@163.com
 * 实际建造者
 */
public class HighHouseBuilder  extends HouseAbsBuilder{
    @Override
    protected void bisicBuild() {
        house.setBisic(5.8);
    }

    @Override
    protected void wallsBuild() {
        house.setWalls("白色");
    }

    @Override
    protected void roofedBuild() {
        house.setRoofed("落地窗");

    }
}
