package com.yzg.builder;

/**
 * @author misterWei
 * @create 2020年03月22号:00点55分
 * @mailbox forcontinue@163.com
 * 用来测试
 */
public class TestBuild {
    public static void main(String[] args) {
        HouseDirector houseDirector = new HouseDirector(new HighHouseBuilder());
        System.out.println(houseDirector.createHouse());
        houseDirector.setHouseAbsBuilder(new VillaHouseBuilder());
        System.out.println(houseDirector.createHouse());
    }
}
