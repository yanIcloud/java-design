package com.yzg.builder;

/**
 * @author misterWei
 * @create 2020年03月22号:00点46分
 * @mailbox forcontinue@163.com
 * 抽象建造者
 */
public abstract class HouseAbsBuilder {
    protected House house = new House();

    protected abstract void bisicBuild();
    protected abstract void wallsBuild();
    protected abstract void roofedBuild();

    public House build(){
        return house;
    }


}
