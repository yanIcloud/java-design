package com.yzg.builder;

/**
 * @author misterWei
 * @create 2020年03月22号:00点52分
 * @mailbox forcontinue@163.com
 * 指挥者
 */
public class HouseDirector {
    private HouseAbsBuilder houseAbsBuilder;

    public HouseDirector(HouseAbsBuilder houseAbsBuilder){
        this.houseAbsBuilder = houseAbsBuilder;
    }

    public void setHouseAbsBuilder(HouseAbsBuilder houseAbsBuilder){
        this.houseAbsBuilder = houseAbsBuilder;
    }
   //具体怎么创建房子的流程交给指挥者
    public House createHouse(){
        houseAbsBuilder.bisicBuild();
        houseAbsBuilder.wallsBuild();
        houseAbsBuilder.roofedBuild();
        return houseAbsBuilder.build();
    }
}
