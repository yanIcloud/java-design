package com.yzg.builder;

/**
 * @author misterWei
 * @create 2020年03月22号:00点49分
 * @mailbox forcontinue@163.com
 * 实际建造者
 */
public class VillaHouseBuilder extends HouseAbsBuilder {
    @Override
    protected void bisicBuild() {
        house.setBisic(1.8);

    }

    @Override
    protected void wallsBuild() {
        house.setWalls("黑色");

    }

    @Override
    protected void roofedBuild() {
        house.setRoofed("天花板");

    }
}
