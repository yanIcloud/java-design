package com.yzg.builder;

/**
 * @author misterWei
 * @create 2020年03月22号:00点43分
 * @mailbox forcontinue@163.com
 * 具体的房子实例化
 */
public class House {
    @Override
    public String toString() {
        return "House{" +
                "bisic=" + bisic +
                ", walls='" + walls + '\'' +
                ", roofed='" + roofed + '\'' +
                '}';
    }

    //地基度
    private Double bisic;

    //墙
    private String walls;

    //屋顶
    private String roofed;

    public Double getBisic() {
        return bisic;
    }

    public void setBisic(Double bisic) {
        this.bisic = bisic;
    }

    public String getWalls() {
        return walls;
    }

    public void setWalls(String walls) {
        this.walls = walls;
    }

    public String getRoofed() {
        return roofed;
    }

    public void setRoofed(String roofed) {
        this.roofed = roofed;
    }
}
