package com.yzg.chain;

/**
 * @author misterWei
 * @create 2020年04月06号:00点26分
 * @mailbox forcontinue@163.com
 */
public abstract class ApprovalChain {
    protected ApprovalChain approvalChain;

    public ApprovalChain(ApprovalChain approvalChain) {
        this.approvalChain = approvalChain;
    }

    public abstract void doProcess(int money);
}
