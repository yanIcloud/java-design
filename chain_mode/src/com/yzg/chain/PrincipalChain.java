package com.yzg.chain;

/**
 * @author misterWei
 * @create 2020年04月06号:00点32分
 * @mailbox forcontinue@163.com
 */
public class PrincipalChain extends ApprovalChain {

    public PrincipalChain(ApprovalChain approvalChain) {
        super(approvalChain);
    }

    @Override
    public void doProcess(int money) {
     if (money >= 5000){
         System.out.println("校长审批了...");
     }
    }
}
