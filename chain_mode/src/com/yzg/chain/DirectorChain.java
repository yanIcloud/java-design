package com.yzg.chain;

/**
 * @author misterWei
 * @create 2020年04月06号:00点30分
 * @mailbox forcontinue@163.com
 */
public class DirectorChain extends ApprovalChain {
    public DirectorChain(ApprovalChain approvalChain) {
        super(approvalChain);
    }

    @Override
    public void doProcess(int money) {
         if (money >= 1000 && money <= 5000){
             System.out.println("教导主任批准了");
         }else{
             //调用下一次链
             approvalChain.doProcess(money);
         }
    }
}
