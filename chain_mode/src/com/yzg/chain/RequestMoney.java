package com.yzg.chain;

/**
 * @author misterWei
 * @create 2020年04月06号:00点32分
 * @mailbox forcontinue@163.com
 */
public class RequestMoney {
    private int money;
    private ApprovalChain approvalChain ;

    public RequestMoney(int money) {
        this.money = money;
    }

    public void setMoney(int money) {
        this.money = money;
    }

    public void doPost(){
        //链式调用,也可以不用这样调用,笔者为了简单描述才这样去构造
      approvalChain  = new TeacherChain(new DirectorChain(new PrincipalChain(null)));
        approvalChain.doProcess(money);
    }
}
