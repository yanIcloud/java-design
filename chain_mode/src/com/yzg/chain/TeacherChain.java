package com.yzg.chain;

/**
 * @author misterWei
 * @create 2020年04月06号:00点27分
 * @mailbox forcontinue@163.com
 */
public class TeacherChain extends ApprovalChain {


    public TeacherChain(ApprovalChain approvalChain) {
        super(approvalChain);
    }



    @Override
    public void doProcess(int money) {
       if (money<= 1000){
           System.out.println("老师处理了");
       }else{
           //调用下一次链
         approvalChain.doProcess(money);
       }
    }
}
