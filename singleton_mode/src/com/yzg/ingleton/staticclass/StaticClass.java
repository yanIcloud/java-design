package com.yzg.ingleton.staticclass;

/**
 * @author misterWei
 * @create 2020年03月15号:20点30分
 * @mailbox forcontinue@163.com
 * 单例模式 - 静态内部类
 */
public class StaticClass {

    public static void main(String[] args) {
        ClassLoaderStatic instance = ClassLoaderStatic.getInstance();
        ClassLoaderStatic instance1 = ClassLoaderStatic.getInstance();
        System.out.println(instance == instance1);
    }
}

class ClassLoaderStatic {

    private ClassLoaderStatic() {
    }

    //采用静态内部类的方式加载,采用的是JVM类加载机制
   private static class ClassLoaderStaticInstance {
        private static final ClassLoaderStatic  INSTANCE = new ClassLoaderStatic();
    }

    public static ClassLoaderStatic getInstance(){
        return ClassLoaderStaticInstance.INSTANCE;
    }
}
