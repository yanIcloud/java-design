package com.yzg.ingleton.hungry;

/**
 * @author misterWei
 * @create 2020年03月15号:00点23分
 * @mailbox forcontinue@163.com
 *
 * 单例模式- 懒汉式_静态常量
 */
public class Constant {

    public static void main(String[] args) {
        Slacker instance = Slacker.getInstance();
        Slacker instance1 = Slacker.getInstance();
        System.out.println(instance == instance1);
    }


}

class Slacker{

    //提供静态常量
    private  final  static Slacker slacker= new Slacker();

    //私有化构造方法
    private Slacker(){

    }

    //对外提供静态的获取实例方法
    public static Slacker getInstance(){
        return slacker;
    }

}