package com.yzg.ingleton.hungry;

/**
 * @author misterWei
 * @create 2020年03月15号:01点06分
 * @mailbox forcontinue@163.com
 *
 * 单例模式- 饿汉式_静态代码块
 */
public class CodeBlock {

    public static void main(String[] args) {
        Block instance = Block.getInstance();
        Block instance1 = Block.getInstance();
        System.out.println(instance == instance1);
    }
}

class Block{

    //提供初始化静态变量
    private final static Block block;

    //私有化构造方法
    private Block(){

    }

    //静态代码块执行器
    static {
        block = new Block();
    }

    //对外提供静态的获取实例方法
    public static Block getInstance(){
        return block;
    }
}