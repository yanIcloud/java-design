package com.yzg.ingleton.lazy;

/**
 * @author misterWei
 * @create 2020年03月15号:01点54分
 * @mailbox forcontinue@163.com
 *
 * 懒汉模式- 懒加载_线程安全
 */
public class LazySecurity {

    public static void main(String[] args) {
        LazySyn instance = LazySyn.getInstance();
        LazySyn instance1 = LazySyn.getInstance();
        System.out.println(instance == instance1);

    }


}

class LazySyn{
    private static LazySyn lazy;

    private LazySyn(){

    }

    public static synchronized LazySyn getInstance(){
        if (lazy == null){
            lazy = new LazySyn();
        }
        return lazy;
    }

}
