package com.yzg.ingleton.lazy;

/**
 * @author misterWei
 * @create 2020年03月15号:01点54分
 * @mailbox forcontinue@163.com
 *
 * 懒汉模式- 懒加载_线程不安全
 */
public class LazyNoSecurity {

    public static void main(String[] args) {
        Lazy instance = Lazy.getInstance();
        Lazy instance1 = Lazy.getInstance();
        System.out.println(instance == instance1);

    }


}

class Lazy{
    private static Lazy lazy;

    private Lazy(){

    }

    public static Lazy getInstance(){
        if (lazy == null){
            lazy = new Lazy();
        }
        return lazy;
    }

}
