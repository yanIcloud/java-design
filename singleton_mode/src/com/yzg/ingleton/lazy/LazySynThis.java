package com.yzg.ingleton.lazy;

/**
 * @author misterWei
 * @create 2020年03月15号:01点54分
 * @mailbox forcontinue@163.com
 *
 * 懒汉模式- 懒加载_线程安全-- > 错误示例
 */
public class LazySynThis {

    public static void main(String[] args) {
        LazySyn instance = LazySyn.getInstance();
        LazySyn instance1 = LazySyn.getInstance();
        System.out.println(instance == instance1);

    }


}

class LazyThis{
    private static LazyThis lazy;

    private LazyThis(){

    }

    public static  LazyThis getInstance(){
        if (lazy == null){
            synchronized (LazyThis.class){
                lazy = new LazyThis();
            }
        }
        return lazy;
    }

}
