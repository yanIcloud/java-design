package com.yzg.ingleton.enumerate;

/**
 * @author misterWei
 * @create 2020年03月15号:20点55分
 * @mailbox forcontinue@163.com
 *单例模式 - 枚举模式
 */
public class EnumSngleton {

    public static void main(String[] args) {
        Sngleton instance = Sngleton.INSTANCE;
        Sngleton instance1 = Sngleton.INSTANCE;
        System.out.println(instance == instance1);
    }
}

enum Sngleton{
    //枚举特性,能够返回当前实例
    INSTANCE;
    public void printKey(){
        System.out.println(INSTANCE);
    }
}
