package com.yzg.ingleton.doublecheck;

/**
 * @author misterWei
 * @create 2020年03月15号:20点45分
 * @mailbox forcontinue@163.com
 *
 * 单例模式 - 双重检查
 */
public class Person {

    public static void main(String[] args) {
        SuperMan instance = SuperMan.getInstance();
        SuperMan instance1 = SuperMan.getInstance();
        System.out.println(instance == instance1);

    }
}

class SuperMan{

    //注意这里是  volatile 关键字,保证这个值的可见性,实时更改就会立即加载到主存当中
  private static volatile SuperMan superMan;

  private SuperMan(){

  }

  public static SuperMan getInstance(){

      //第一次判断
      if (superMan == null){

          synchronized (SuperMan.class){
             //加锁之后第二次判断 加了 volatile 这个值对于多个同步线程来说是可见的
              if (superMan == null){
                  superMan = new SuperMan();
              }

          }
      }

      return superMan;
  }

}
