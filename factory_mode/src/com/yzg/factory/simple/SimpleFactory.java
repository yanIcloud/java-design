package com.yzg.factory.simple;

import com.yzg.factory.simple.entity.Animal;
import com.yzg.factory.simple.entity.Dog;
import com.yzg.factory.simple.entity.Pig;

/**
 * @author misterWei
 * @create 2020年03月16号:21点25分
 * @mailbox forcontinue@163.com
 *
 * 工厂模式 - 简单工厂
 *
 */
public class SimpleFactory {

    public static Animal createAdminal(Class className){
        Object animal = null;
        try {
            animal = className.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (animal instanceof Animal) {
            return (Animal) animal;
        }else{
            throw new RuntimeException("animal Type Error");
        }

    }

    //测试方法
    public static void main(String[] args) {

        Animal adminal = SimpleFactory.createAdminal(Dog.class);
        adminal.init();
        adminal.aniMalName();
        Animal adminal1 = SimpleFactory.createAdminal(Pig.class);
        adminal1.init();
        adminal1.aniMalName();

    }


}
