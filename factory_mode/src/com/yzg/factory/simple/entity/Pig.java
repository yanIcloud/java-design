package com.yzg.factory.simple.entity;

/**
 * @author misterWei
 * @create 2020年03月16号:22点23分
 * @mailbox forcontinue@163.com
 */
public class Pig extends Animal {
    @Override
    public void init() {
        super.zooName = "猪";
    }
}
