package com.yzg.factory.simple.entity;

/**
 * @author misterWei
 * @create 2020年03月16号:22点20分
 * @mailbox forcontinue@163.com
 *
 * 这是一个动物园的抽象类,用来管理所有动物的行为话
 */
public abstract class Animal {

    //动物名称
    protected String zooName;

    //用来初始化动物方法
    public abstract void init();



    public void aniMalName(){
        System.out.println("动物名称: "+zooName);
    }
}
