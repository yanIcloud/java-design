package com.yzg.flyweight.des;

/**
 * @author misterWei
 * @create 2020年03月29号:22点18分
 * @mailbox forcontinue@163.com
 */
public class CodeSource {

    public static void main(String[] args) {
        /**
         * 这种写法代表什么意思
         * 在JMM 模型中 常量池会多一个 以s为引用的 1 的常量存储,而 s1 栈内动态链接到堆地址
         * 1.在JMM中两者存储不一样,一个常量池,一个堆内存
         * 2.其实更细一点, s 在常量池已经有了, 那么这个 s1虽然是有一个堆空间的但是 如果这个字符串在常量池已经存在,那么这个堆空间会指向这个常量,如果这个常量改变,那么堆内存会创建出新的实例
         */
        String s = "1";
        String s1 = new String("1");

        /**
         * 说明
         * i1 == i2 是 true
         * 因为 源码中会初始化 - 128 ~ 127 如果这个值在这个范围内就会从缓冲区获取
         *
         * i3 == i4 是false
         * 因为源码中没有 200 的这个范围,所以就new 出来新的对象了
         */

        Integer i1 = Integer.valueOf(127);
        Integer i2 = Integer.valueOf(127);
        System.out.println(i1 == i2); // true

        Integer i3 = Integer.valueOf(200);
        Integer i4 = Integer.valueOf(200);
        System.out.println(i3 == i4); //false

    }
}
