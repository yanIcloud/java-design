package com.yzg.flyweight;

import java.util.HashMap;
import java.util.Map;

/**
 * @author misterWei
 * @create 2020年03月29号:22点30分
 * @mailbox forcontinue@163.com
 */
public class WebSiteFactory {

    private Map<String,FlyweightAbsWebSite> pool = new HashMap<>();


    public FlyweightAbsWebSite getType(String type){

        if (!pool.containsKey(type)){
            FlyweightAbsWebSite webSite = new DefaultWebSite(type);
            pool.put(type,webSite);
        }
        return pool.get(type);




    }

    public int size(){
        return pool.size();
    }

}
