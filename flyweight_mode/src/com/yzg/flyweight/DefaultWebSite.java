package com.yzg.flyweight;

/**
 * @author misterWei
 * @create 2020年03月29号:22点28分
 * @mailbox forcontinue@163.com
 */
public class DefaultWebSite extends FlyweightAbsWebSite {
    private String type;

    public DefaultWebSite(String type) {
        this.type = type;
    }

    @Override
    public void showWebSite(User user) {
        System.out.println(user.getName()+" ---> "+ type);

    }
}
