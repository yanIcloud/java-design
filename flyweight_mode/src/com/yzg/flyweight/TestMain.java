package com.yzg.flyweight;

/**
 * @author misterWei
 * @create 2020年03月29号:22点17分
 * @mailbox forcontinue@163.com
 */
public class TestMain {

    public static void main(String[] args) {
        /**
         * User 相当于外部传统, 享元对象的共同点抽离
         * Type 相当于内部状态, 享元对象内部维护
         */
        WebSiteFactory factory = new WebSiteFactory();
        FlyweightAbsWebSite news = factory.getType("新闻");
        news.showWebSite(new User("张三"));

        FlyweightAbsWebSite news2 = factory.getType("新闻");
        news2.showWebSite(new User("李四"));

        FlyweightAbsWebSite lock = factory.getType("电影");
        lock.showWebSite(new User("王五"));

        System.out.println(factory.size());
    }
}
