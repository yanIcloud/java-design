package com.yzg.flyweight;

/**
 * @author misterWei
 * @create 2020年03月29号:22点26分
 * @mailbox forcontinue@163.com
 */
public abstract class FlyweightAbsWebSite {

    public abstract void showWebSite(User user);
}
