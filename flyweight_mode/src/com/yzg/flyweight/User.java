package com.yzg.flyweight;

/**
 * @author misterWei
 * @create 2020年03月29号:22点29分
 * @mailbox forcontinue@163.com
 */
public class User {

    private String name;

    public User(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }


}
