package com.yzg.observable;

/**
 * @author misterWei
 * @create 2020年04月05号:01点13分
 * @mailbox forcontinue@163.com
 */
public class BaiduObServer implements ObServer {
    @Override
    public void upData(int temperature, int humidity) {

        System.out.println("百度 --> 气温 = "+ temperature);
        System.out.println("百度 --> 始湿度= "+ humidity);

    }
}
