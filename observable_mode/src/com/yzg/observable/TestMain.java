package com.yzg.observable;

/**
 * @author misterWei
 * @create 2020年04月05号:01点06分
 * @mailbox forcontinue@163.com
 */
public class TestMain {

    public static void main(String[] args) {

        WeatherSubject weatherSubject = new WeatherSubject();


        //观察者 百度
        BaiduObServer baiduObServer = new BaiduObServer();
        weatherSubject.registered(baiduObServer);

        //观察者阿里巴巴
        AlibabaObServer alibabaObServer = new AlibabaObServer();
        weatherSubject.registered(alibabaObServer);

        weatherSubject.upData(10,30);


        weatherSubject.upData(221,45);

    }

}
