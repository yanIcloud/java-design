package com.yzg.observable;

/**
 * @author misterWei
 * @create 2020年04月05号:01点14分
 * @mailbox forcontinue@163.com
 */
public class AlibabaObServer implements ObServer {
    @Override
    public void upData(int temperature, int humidity) {
        System.out.println("阿里 --> 气温 = "+ temperature);
        System.out.println("阿里 --> 始湿度= "+ humidity);
    }
}
