package com.yzg.observable;

import java.util.ArrayList;
import java.util.List;

/**
 * @author misterWei
 * @create 2020年04月05号:01点09分
 * @mailbox forcontinue@163.com
 */
public class WeatherSubject implements Subject {
    private List<ObServer> obServers = new ArrayList<>();
    private int temperature;
    private int humidity;

    public void upData(int temperature,int humidity){
        this.humidity = humidity;
        this.temperature = temperature;
        notifyAlls();
    }

    public WeatherSubject() {

    }

    @Override
    public void registered(ObServer obServer) {
        obServers.add(obServer);
    }

    @Override
    public void remove(ObServer obServer) {
        if (obServers.contains(obServer)) {
            obServers.remove(obServer);
        }

    }

    @Override
    public void notifyAlls() {
        obServers.forEach(obServer -> {
            obServer.upData(this.temperature,this.humidity);
        });


    }
}
