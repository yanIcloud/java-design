package com.yzg.observable;

/**
 * @author misterWei
 * @create 2020年04月05号:01点07分
 * @mailbox forcontinue@163.com
 */
public interface ObServer {
    void upData(int temperature,int humidity);
}
