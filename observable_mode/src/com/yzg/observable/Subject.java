package com.yzg.observable;

/**
 * @author misterWei
 * @create 2020年04月05号:01点06分
 * @mailbox forcontinue@163.com
 *
 * 数据来源体规范
 */
public interface Subject {

    void registered(ObServer obServer);
    void remove(ObServer obServer);
    void notifyAlls();
}
