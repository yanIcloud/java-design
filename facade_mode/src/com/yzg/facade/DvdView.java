package com.yzg.facade;

/**
 * @author misterWei
 * @create 2020年03月29号:19点48分
 * @mailbox forcontinue@163.com
 */
public class DvdView {

    private static final DvdView DVD_VIEW = new DvdView();
    private DvdView(){
    }

    public  static  DvdView instantiate(){
        return DVD_VIEW;
    }


    public void open(){
        System.out.println("DVD 开机了");
    }

    public void stop(){
        System.out.println("DVD 暂停了");
    }

    public void close(){
        System.out.println("DVD 关闭了");
    }
}
