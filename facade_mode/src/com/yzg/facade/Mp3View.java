package com.yzg.facade;

/**
 * @author misterWei
 * @create 2020年03月29号:19点49分
 * @mailbox forcontinue@163.com
 */
public class Mp3View {

    private static final Mp3View MP_3_VIEW = new Mp3View();
    private Mp3View(){
    }

    public  static  Mp3View instantiate(){
        return MP_3_VIEW;
    }

    public void open(){
        System.out.println("Mp3 开机了");
    }

    public void stop(){
        System.out.println("Mp3 暂停了");
    }

    public void close(){
        System.out.println("Mp3 关闭了");
    }
}
