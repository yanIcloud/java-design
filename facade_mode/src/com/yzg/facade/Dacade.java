package com.yzg.facade;

/**
 * @author misterWei
 * @create 2020年03月29号:19点47分
 * @mailbox forcontinue@163.com
 *
 * 享元模式的实例化 -- 对复杂的调用关系进行了封装.
 */
public class Dacade {
    private ComputerView computerView;
    private DvdView dvdView;
    private Mp3View mp3View;

    public Dacade(ComputerView computerView, DvdView dvdView, Mp3View mp3View) {
        this.computerView = computerView;
        this.dvdView = dvdView;
        this.mp3View = mp3View;
    }

    public Dacade(){
        this.computerView = ComputerView.instantiate();
        this.dvdView = DvdView.instantiate();
        this.mp3View = Mp3View.instantiate();
    }

    public void open(){
        computerView.open();
        mp3View.open();
        dvdView.open();

    }

    public void close(){
        computerView.close();
        mp3View.close();
        dvdView.close();
    }

    public void stop(){
        computerView.stop();
        mp3View.stop();
        dvdView.stop();
    }
}
