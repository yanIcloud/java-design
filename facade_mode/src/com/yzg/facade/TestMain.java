package com.yzg.facade;

/**
 * @author misterWei
 * @create 2020年03月29号:19点47分
 * @mailbox forcontinue@163.com
 */
public class TestMain {

    public static void main(String[] args) {

        Dacade dacade = new Dacade();
        dacade.open();
        dacade.stop();
        dacade.close();
    }
}
