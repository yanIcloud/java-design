package com.yzg.facade;

/**
 * @author misterWei
 * @create 2020年03月29号:19点50分
 * @mailbox forcontinue@163.com
 */
public class ComputerView {

    private static final ComputerView COMPUTER_VIEW = new ComputerView();
    private ComputerView(){
    }

    public  static  ComputerView instantiate(){
        return COMPUTER_VIEW;
    }


    public void open(){
        System.out.println("Computer 开机了");
    }

    public void stop(){
        System.out.println("Computer 暂停了");
    }

    public void close(){
        System.out.println("Computer 关闭了");
    }
}
