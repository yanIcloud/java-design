package com.yzg.strategy;

/**
 * @author misterWei
 * @create 2020年04月05号:23点33分
 * @mailbox forcontinue@163.com
 *
 * 是否会游泳的策略
 */
public interface FlyStrategy {

    void fly();
}
