package com.yzg.strategy;

/**
 * @author misterWei
 * @create 2020年04月05号:23点29分
 * @mailbox forcontinue@163.com
 */
public class TestMain {

    public static void main(String[] args) {
       Duck duck = new BeijingDuck(new YesFlyStrategy());
       duck.fly();
       duck = new ToyDuck(new NoFlyStrategy());
       duck.fly();
    }
}
