package com.yzg.strategy;

/**
 * @author misterWei
 * @create 2020年04月05号:23点34分
 * @mailbox forcontinue@163.com
 */
public class ToyDuck extends Duck {

    private FlyStrategy flyStrategy;

    public ToyDuck(FlyStrategy flyStrategy) {
        this.flyStrategy = flyStrategy;
    }

    @Override
    protected void fly() {
        flyStrategy.fly();
    }
}
