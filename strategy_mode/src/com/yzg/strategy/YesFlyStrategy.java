package com.yzg.strategy;

/**
 * @author misterWei
 * @create 2020年04月05号:23点33分
 * @mailbox forcontinue@163.com
 */
public class YesFlyStrategy implements FlyStrategy {
    @Override
    public void fly() {
        System.out.println("鸭子会游泳");
    }
}
