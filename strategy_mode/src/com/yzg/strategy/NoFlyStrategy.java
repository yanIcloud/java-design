package com.yzg.strategy;

/**
 * @author misterWei
 * @create 2020年04月05号:23点34分
 * @mailbox forcontinue@163.com
 */
public class NoFlyStrategy implements FlyStrategy {
    @Override
    public void fly() {
        System.out.println("鸭子不会游泳");
    }
}
