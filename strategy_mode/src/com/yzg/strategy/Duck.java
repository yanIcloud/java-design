package com.yzg.strategy;

/**
 * @author misterWei
 * @create 2020年04月05号:23点29分
 * @mailbox forcontinue@163.com
 *
 * 鸭子抽象
 */
public abstract class Duck {
    //鸭子是否会飞翔由子类决定
    protected abstract void fly();

    protected void swim(){
        System.out.println("鸭子会游泳");
    }

}
