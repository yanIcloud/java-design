package com.yzg.bridge;

/**
 * @author misterWei
 * @create 2020年03月22号:21点13分
 * @mailbox forcontinue@163.com
 */
public class FoldPhone extends Phone {

    public FoldPhone(Brand brand) {
        super(brand);
    }

    @Override
    protected void open() {
      super.open();
        System.out.println("折叠");

    }

    @Override
    protected void close() {
        super.close();
        System.out.println("折叠");


    }

    @Override
    protected void call() {
        super.call();
        System.out.println("折叠");


    }
}
