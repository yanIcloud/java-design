package com.yzg.bridge;

/**
 * @author misterWei
 * @create 2020年03月22号:19点07分
 * @mailbox forcontinue@163.com
 */
public class TestMain {
    public static void main(String[] args) {
        Phone phone = new FlatPhone(new Oppo());
        phone.open();
        phone.call();
        phone.close();
    }
}
