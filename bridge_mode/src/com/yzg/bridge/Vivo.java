package com.yzg.bridge;

/**
 * @author misterWei
 * @create 2020年03月22号:21点11分
 * @mailbox forcontinue@163.com
 */
public class Vivo implements Brand {
    @Override
    public String brandName() {
        return "Vivo";
    }
}
