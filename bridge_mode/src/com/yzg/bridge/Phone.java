package com.yzg.bridge;

/**
 * @author misterWei
 * @create 2020年03月22号:21点11分
 * @mailbox forcontinue@163.com
 * 具体的抽象
 */
public abstract class Phone {
    private Brand brand;
    public Phone(Brand brand){
        this.brand = brand;
    }
    protected  void open(){
        System.out.println(brand.brandName()+" 开机");

    }
    protected  void close(){
        System.out.println(brand.brandName()+" 关机");


    }
    protected  void call(){
        System.out.println(brand.brandName()+" 打电话");


    }

}
