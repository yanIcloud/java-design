package com.yzg.bridge;

/**
 * @author misterWei
 * @create 2020年03月22号:21点17分
 * @mailbox forcontinue@163.com
 */
public class FlatPhone extends Phone {
    public FlatPhone(Brand brand) {
        super(brand);
    }

    @Override
    protected void open() {
        super.open();
        System.out.println("平板");

    }

    @Override
    protected void close() {
        super.close();
        System.out.println("平板");

    }

    @Override
    protected void call() {
        super.call();
        System.out.println("平板");
    }
}
