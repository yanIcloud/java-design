package com.yzg.bridge;

/**
 * @author misterWei
 * @create 2020年03月22号:21点10分
 * @mailbox forcontinue@163.com
 */
public class Oppo implements Brand {
    @Override
    public String brandName() {
        return "Oppo";
    }
}
