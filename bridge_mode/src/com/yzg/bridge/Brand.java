package com.yzg.bridge;

/**
 * @author misterWei
 * @create 2020年03月22号:21点09分
 * @mailbox forcontinue@163.com
 *
 * 具体的实现规则
 */
public interface Brand {
    String brandName();
}
