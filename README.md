# java-design

#### 介绍
Java 设计模式大全

#### 软件架构
23 种设计模式的落地实现

#### 项目说明
1.次项目采用的是传统的java项目,并没有使用maven或者gradle等项目管理
#### 模块说明
> 创建型模式
1. singleton_mode - 单例模式的八大种写法
2. factory_mode - 工厂模式+抽象工厂模式(2种)
3. prototype_mode - 原型模式
4. builder_mode - 建造者模式
> 结构型模式
1. adapter_mode -  适配器模式
2. bridge_mode - 桥接模式
3. decorator_model - 装饰者模式
4. combination_mode - 组合模式
5. facade_mode - 外观模式(门面模式)
6. flyweight_mode - 享元模式
7. proxy_mode - 代理模式
> 行为式模式
1. template_mode - 模板方法模式
2. command_mode - 命令模式
3. visitor_mode - 访问者模式
4. Iterator_mode - 迭代器模式
5. observable_mode - 观察者模式
6. mediator_mode - 中介者模式
7. memo_mode - 备忘录模式
8. explanation_mode - 解释器模式
9. status_mode - 状态模式
10. strategy_mode - 策略模式
11. chain_mode - 责任链模式



#### 参与贡献
yanwei