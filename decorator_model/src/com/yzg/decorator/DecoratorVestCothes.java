package com.yzg.decorator;

/**
 * @author misterWei
 * @create 2020年03月27号:22点26分
 * @mailbox forcontinue@163.com
 * 马甲的增加
 */
public class DecoratorVestCothes extends DecoratorCothes {

    public DecoratorVestCothes(Cothes cothes) {
        super(cothes);
    }

    @Override
    public String getButton() {
        return cothes.getButton()+"增加";
    }

    @Override
    public Float getSize() {
        return cothes.getSize()+3F;
    }
}
