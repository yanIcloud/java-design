package com.yzg.decorator;

/**
 * @author misterWei
 * @create 2020年03月27号:22点15分
 * @mailbox forcontinue@163.com
 */
public class TestMain {

    public static void main(String[] args) {
        Cothes cothes = new VestCothes();
        //动态增加
        DecoratorCothes decoratorCothes = new DecoratorVestCothes(cothes);
        System.out.println(decoratorCothes.getSize());
        System.out.println(decoratorCothes.getButton());

    }
}
