package com.yzg.decorator;

/**
 * @author misterWei
 * @create 2020年03月27号:22点17分
 * @mailbox forcontinue@163.com
 *
 * 卫衣
 */
public class SweaterCothes implements Cothes {
    @Override
    public String getButton() {
        return "7.0";
    }

    @Override
    public Float getSize() {
        return 1.0F;
    }
}
