package com.yzg.decorator;

/**
 * @author misterWei
 * @create 2020年03月27号:22点15分
 * @mailbox forcontinue@163.com
 *
 * 这是衣服的抽象
 */
public interface Cothes {

    //衣服多少纽扣
    String getButton();

    //衣服多大尺寸
    Float getSize();
}
