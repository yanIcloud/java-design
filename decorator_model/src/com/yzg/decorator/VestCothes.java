package com.yzg.decorator;

/**
 * @author misterWei
 * @create 2020年03月27号:22点17分
 * @mailbox forcontinue@163.com
 *
 * 马甲
 */
public class VestCothes implements Cothes {
    @Override
    public String getButton() {
        return "4.5";
    }

    @Override
    public Float getSize() {
        return 8.0F;
    }
}
