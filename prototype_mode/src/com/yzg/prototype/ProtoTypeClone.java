package com.yzg.prototype;

/**
 * @author misterWei
 * @create 2020年03月21号:00点34分
 * @mailbox forcontinue@163.com
 * 如果需要使用clone就必须实现 Cloneable接口 - 这是一种浅克隆
 */

public class ProtoTypeClone implements Cloneable{

    private String classId;
    private String classme;

    public String getClassId() {
        return classId;
    }

    public String getClassme() {
        return classme;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public void setClassme(String classme) {
        this.classme = classme;
    }

    @Override
    public String toString() {
        return "ProtoTypeClone{" +
                "classId='" + classId + '\'' +
                ", classme='" + classme + '\'' +
                '}';
    }

    @Override
    protected ProtoTypeClone clone() throws CloneNotSupportedException {
        ProtoTypeClone protoTypeClone = null;
        protoTypeClone= (ProtoTypeClone) super.clone();
        return protoTypeClone;
    }

    /**
     * 测试
     * @param args
     */
    public static void main(String[] args) throws CloneNotSupportedException {
        ProtoTypeClone protoTypeClone = new ProtoTypeClone();
        protoTypeClone.setClassId("1");
        protoTypeClone.setClassme("ProtoTypeClone");
        ProtoTypeClone clone = protoTypeClone.clone();
        //完成浅克隆
        System.out.println(protoTypeClone);
        System.out.println(clone);

    }
}
