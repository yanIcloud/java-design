package com.yzg.prototype.deepcopy;

import java.io.Serializable;

/**
 * @author misterWei
 * @create 2020年03月21号:01点25分
 * @mailbox forcontinue@163.com
 * 这是一个子属性类
 */
public class ProtoTypeCloneChild implements Cloneable,Serializable {
    private String classId;
    private String calssName;

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getCalssName() {
        return calssName;
    }

    public void setCalssName(String calssName) {
        this.calssName = calssName;
    }

    @Override
    public String toString() {
        return "ProtoTypeCloneChild{" +
                "classId='" + classId + '\'' +
                ", calssName='" + calssName + '\'' +
                '}';
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
