package com.yzg.prototype.deepcopy;

import java.io.*;

/**
 * @author misterWei
 * @create 2020年03月21号:01点26分
 * @mailbox forcontinue@163.com
 * 这是一个深拷贝的实例demo -1 采用clone方法进行深克隆,2.采用序列化流方式深克隆
 */
public class ProtoTypeClone implements Cloneable, Serializable {

    private String classId;
    private String className;
    private ProtoTypeCloneChild protoTypeCloneChild;

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public ProtoTypeCloneChild getProtoTypeCloneChild() {
        return protoTypeCloneChild;
    }

    public void setProtoTypeCloneChild(ProtoTypeCloneChild protoTypeCloneChild) {
        this.protoTypeCloneChild = protoTypeCloneChild;
    }

    @Override
    public String toString() {
        return "ProtoTypeClone{" +
                "classId='" + classId + '\'' +
                ", className='" + className + '\'' +
                ", protoTypeCloneChild=" + protoTypeCloneChild +
                '}';
    }

    //深克隆 - 1  使用clone方式去做,效果能实现,伸缩性不太好
    @Override
    protected ProtoTypeClone clone() throws CloneNotSupportedException {
        ProtoTypeClone protoTypeClone;
        protoTypeClone = (ProtoTypeClone) super.clone();
        //这是关键点,也就是说使用clone的方式的话,那么子类也需要进行clone所以都要实现对应的接口
        protoTypeClone.protoTypeCloneChild = (ProtoTypeCloneChild) protoTypeCloneChild.clone();
        return protoTypeClone;
    }

    // 深克隆二,序列化流方式
    public ProtoTypeClone deepClone(){
        ByteArrayOutputStream bos = null;
        ObjectOutputStream oos = null;
        ByteArrayInputStream bis = null;
        ObjectInputStream ois = null;
        try {

            //序列化
            bos = new ByteArrayOutputStream();
            oos = new ObjectOutputStream(bos);
            //写出当前实例
            oos.writeObject(this);

            //反序列化
            bis = new ByteArrayInputStream(bos.toByteArray());
            ois = new ObjectInputStream(bis);
            Object cloneObj = ois.readObject();
            return (ProtoTypeClone) cloneObj;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }finally {
            try {
                bos.close();
                oos.close();
                bis.close();
                ois.close();
            }catch (Exception ex){
                ex.printStackTrace();
            }
        }


    }
/*    *//**
     * 测试 第一种clone方式
     *
     * @param args
     *//*
    public static void main(String[] args) throws CloneNotSupportedException {
        ProtoTypeClone protoTypeClone = new ProtoTypeClone();
        protoTypeClone.setClassId("1");
        protoTypeClone.setClassName("ProtoTypeClone");
        ProtoTypeCloneChild protoTypeCloneChild = new ProtoTypeCloneChild();
        protoTypeCloneChild.setClassId("2");
        protoTypeCloneChild.setCalssName("ProtoTypeCloneChild");
        protoTypeClone.setProtoTypeCloneChild(protoTypeCloneChild);
        //克隆
        ProtoTypeClone clone = protoTypeClone.clone();
        clone.getProtoTypeCloneChild().setCalssName("测试");
        System.out.println(protoTypeClone);
        System.out.println(clone);
    }*/

    /**
     * 测试 第二种clone方式
     *
     * @param args
     */
    public static void main(String[] args) throws CloneNotSupportedException {
        ProtoTypeClone protoTypeClone = new ProtoTypeClone();
        protoTypeClone.setClassId("1");
        protoTypeClone.setClassName("ProtoTypeClone");
        ProtoTypeCloneChild protoTypeCloneChild = new ProtoTypeCloneChild();
        protoTypeCloneChild.setClassId("2");
        protoTypeCloneChild.setCalssName("ProtoTypeCloneChild");
        protoTypeClone.setProtoTypeCloneChild(protoTypeCloneChild);
        //克隆
        ProtoTypeClone clone = protoTypeClone.deepClone();
        clone.getProtoTypeCloneChild().setCalssName("测试");
        System.out.println(protoTypeClone);
        System.out.println(clone);
    }
}
