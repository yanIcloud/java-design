package com.yzg.template;

/**
 * @author misterWei
 * @create 2020年04月02号:19点15分
 * @mailbox forcontinue@163.com
 *
 * 纯豆浆制作过程 不加辅料
 */
public class ThisSoymilk extends Soymilk {
    @Override
    protected void addSeasoning() {

    }

    @Override
    protected boolean isSeasoning() {

        return true;
    }
}
