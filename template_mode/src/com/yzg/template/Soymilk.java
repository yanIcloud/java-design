package com.yzg.template;

/**
 * @author misterWei
 * @create 2020年04月02号:19点07分
 * @mailbox forcontinue@163.com
 *
 * 这是一个制作豆浆的抽象体
 */
public  abstract class Soymilk {


//不让子类重写
    final void process(){
        readyMilk();
        if (!isSeasoning()) {
            addSeasoning();
        }
        success();
    }


    //第一步,准备大豆
     final void readyMilk(){
        System.out.println("准备大豆");
    }

    //第二部,放入佐料 - 这个方法应该由子类去实现 可能对应的是红豆豆浆,或者绿豆豆浆
    protected abstract void addSeasoning();

    //第三步,加工成功,豆浆出炉
    final void success(){
        System.out.println("豆浆出炉了");
    }



    //提供一个钩子方法,该方法决定了是否启用 加入佐料这个动作
    protected boolean isSeasoning(){
        return false;
    }
}
