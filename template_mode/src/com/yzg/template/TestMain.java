package com.yzg.template;

/**
 * @author misterWei
 * @create 2020年04月02号:19点05分
 * @mailbox forcontinue@163.com
 */
public class TestMain {

    public static void main(String[] args) {
        //红豆豆浆制作
        Soymilk redSoy = new RedBeansSoymilk();
        redSoy.process();

        //绿豆豆浆制作
        Soymilk greenSoy = new GreenBeansSoymilk();
        greenSoy.process();

        //纯豆浆制作 -- 钩子方法
        Soymilk thisSoy = new ThisSoymilk();
        thisSoy.process();
    }
}
